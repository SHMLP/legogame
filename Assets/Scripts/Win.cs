using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Serialization;

public class Win : MonoBehaviour
{
    public GameObject winText;
    public AudioClip winClip;
    public AudioSource source;
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            if (SceneManager.GetSceneByName("Level1") == SceneManager.GetActiveScene())
            {
                SceneManager.LoadScene("Level2");
            }
            else
            {
                GetComponent<MeshRenderer>().enabled = false;
                source.PlayOneShot(winClip);
                other.GetComponent<PlayerController>().IsDying = true;
                winText.SetActive(true);
                StartCoroutine(BackToLevelOne());
            }
        }
    }

    private IEnumerator BackToLevelOne()
    {
        yield return new WaitForSeconds(5);
        SceneManager.LoadScene("Level1");
    }
}
