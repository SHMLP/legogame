using System.Collections.Generic;
using UnityEngine;

public class LifeController : MonoBehaviour
{
    [SerializeField] private GameObject lifePrefab;
    [SerializeField] private int maxNumberLife;

    private int _currentLife;

    private List<GameObject> _lives = new List<GameObject>();
    public void SetLives()
    {
        for (int  i = 0;  i < maxNumberLife;  i++)
        {
            GameObject newLife = Instantiate(lifePrefab,transform);
            _lives.Add(newLife);
        }
        _currentLife = _lives.Count - 1;
    }
    //PIUGPIHJAFÑJKFAÑDIHJAFDÑIFÑAIDFÑADUI
    //Usar lista de stack<> o Queue<>
    public bool ReduceLife()
    {
        //transform.GetChild(_currentLife).gameObject.SetActive(false);
        _lives[_currentLife].SetActive(false);
        if(_currentLife>0)
        {
            _currentLife--;
            return false;
        }
        return true;
    }

}
