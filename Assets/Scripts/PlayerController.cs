using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;

public class PlayerController : MonoBehaviour
{
    [SerializeField] private LifeController lifeController;
    [SerializeField] private TMP_Text levelText;
    
    //[SerializeField] private float speedY;
    //[SerializeField] private float initialPositionY;
    [SerializeField] private float jumpHeight;
    [SerializeField] private float speed; 
    [SerializeField] private float rotationSpeed; 
    

    private Animator _animatorController;
    private CharacterController _characterController;
    private SkinnedMeshRenderer _playerMeshRenderer;

    private float _currentTimeToSit;
    
    private float _playerRotation;
    private float _currentSpeedY;
    private Vector3 _respawnPosition;
    private Vector3 _inputDirection;
    private bool _isRunning;
    private bool _isDying;
    
    public AudioSource soundsSource;
    public AudioClip pickUpObjectsClip;
    public AudioClip jumpClip;
    
    private readonly int _timeToSitAnimationID = Animator.StringToHash("TimeToSit");
    private readonly int _jumpAnimationID = Animator.StringToHash("Jump");
    private readonly int _speedAnimationID = Animator.StringToHash("Speed");

    public bool IsDying
    {
        set => _isDying = value;
    }


    void Awake()
    {
        levelText.text = SceneManager.GetActiveScene().name;
        lifeController.SetLives();
        _respawnPosition = transform.position;
        _animatorController = GetComponent<Animator>();
        _characterController = GetComponent<CharacterController>();
        _playerMeshRenderer = transform.GetChild(1).GetComponent<SkinnedMeshRenderer>();
        
    }

    void Update()
    {
        MovePlayer();
    }

    private void MovePlayer()
    {
        if (_isDying)
        {
            return;
        }
        _inputDirection = new Vector3(0, Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"));

        if (Mathf.Abs(_inputDirection.z) > 0 && !_isRunning)
        {
            _animatorController.SetFloat(_speedAnimationID, Mathf.Abs(_inputDirection.z));
            _currentTimeToSit = 0;
            _animatorController.SetFloat(_timeToSitAnimationID, _currentTimeToSit);
        }
        if (Input.GetButtonDown("Run"))
        {
            _isRunning = true;
            speed *= 2;
            _animatorController.SetFloat(_speedAnimationID, speed);
        }
        if (Input.GetButtonUp("Run"))
        {
            _isRunning = false;
            speed /= 2;
            _animatorController.SetFloat(_speedAnimationID, Mathf.Abs(_inputDirection.z));
        }

        if (Mathf.Abs(_inputDirection.y) > 0)
        {
            _playerRotation += rotationSpeed * Mathf.Sign(_inputDirection.y)*Time.deltaTime;
            _characterController.transform.rotation = Quaternion.Euler(0, _playerRotation, 0);
        }
        if (_characterController.isGrounded && Input.GetButtonDown("Jump"))
        {
            _animatorController.SetTrigger(_jumpAnimationID);
            _currentSpeedY = Mathf.Sqrt(jumpHeight * 2 * 9.8f);
            soundsSource.PlayOneShot(jumpClip);
        }

        _characterController.Move(transform.TransformDirection(new Vector3(0, _currentSpeedY, _inputDirection.z * speed)) * Time.deltaTime);

        if (!_characterController.isGrounded)
        {
            _currentTimeToSit = 0;
            _currentSpeedY += -9.8f * Time.deltaTime;
            _animatorController.SetFloat(_timeToSitAnimationID, _currentTimeToSit);
        }
        else
        {
            _currentTimeToSit += Time.deltaTime;
            _animatorController.SetFloat(_timeToSitAnimationID, _currentTimeToSit);
        }
    }


    private void OnTriggerEnter(Collider other)
    {
        
        if (other.CompareTag("Checkpoint"))
        {
            soundsSource.PlayOneShot(pickUpObjectsClip);
            Destroy(other.gameObject);
            _respawnPosition = other.transform.position;

        }
        if (other.CompareTag("Collectible"))
        {
            soundsSource.PlayOneShot(pickUpObjectsClip);
            Destroy(other.gameObject);
        }
        
        if (other.CompareTag("Lava"))
        {
            CheckReviveOrRestart();
        }

    }

    void CheckReviveOrRestart()
    {
        if (!lifeController.ReduceLife())
        {
            StartCoroutine(Die());
            return;
        }

        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        
    }


    private IEnumerator Die()
    {
        _isDying = true;
        _animatorController.SetFloat(_speedAnimationID, 0);
        _characterController.enabled = false;
        _playerMeshRenderer.enabled = false;
        yield return new WaitForSeconds(1f);
        _currentSpeedY = 0;
        _characterController.transform.position = _respawnPosition;
        _playerMeshRenderer.enabled = true;
        _isDying = false;
        yield return new WaitForEndOfFrame();
        _characterController.enabled = true;
    }
    




}
