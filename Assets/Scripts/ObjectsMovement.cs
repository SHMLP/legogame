using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class ObjectsMovement : MonoBehaviour
{
    [SerializeField] float velocidadCrecimiento;
    [SerializeField] bool escalar, rangoMovimiento;
    [SerializeField] Axis ejeMovimiento;
    [SerializeField] Transform inicio, final;
    void Update()
    {
        if (escalar)
        {
            transform.localScale += new Vector3(0,1f,0) * velocidadCrecimiento * Time.deltaTime;
        }
        switch (ejeMovimiento)
        {
            case Axis.X:
                if (rangoMovimiento)
                {
                    if (transform.position.x > final.position.x && velocidadCrecimiento > 0)
                        velocidadCrecimiento = -velocidadCrecimiento;
                    if (transform.position.x < inicio.position.x && velocidadCrecimiento < 0)
                        velocidadCrecimiento = -velocidadCrecimiento;
                }
                transform.position += new Vector3(0.25f,0, 0) * velocidadCrecimiento * Time.deltaTime;
                break;
            case Axis.Y:
                if (rangoMovimiento)
                {
                    if (transform.position.x > final.position.y && velocidadCrecimiento > 0)
                        velocidadCrecimiento = -velocidadCrecimiento;
                    if (transform.position.x < inicio.position.y && velocidadCrecimiento < 0)
                        velocidadCrecimiento = -velocidadCrecimiento;
                }
                transform.position += new Vector3(0, 0.25f, 0) * velocidadCrecimiento * Time.deltaTime;
                break;
            case Axis.Z:
                if (rangoMovimiento)
                {
                    if (transform.position.x > final.position.z && velocidadCrecimiento > 0)
                        velocidadCrecimiento = -velocidadCrecimiento;
                    if (transform.position.x < inicio.position.z && velocidadCrecimiento < 0)
                        velocidadCrecimiento = -velocidadCrecimiento;
                }
                transform.position += new Vector3(0,0, 0.25f) * velocidadCrecimiento * Time.deltaTime;
                break;
            default:
                break;
        }
    }

    enum Axis
    {
        X,
        Y,
        Z
    }

}
