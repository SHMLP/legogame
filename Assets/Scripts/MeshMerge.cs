using UnityEngine;

public class MeshMerge : MonoBehaviour
{
    [SerializeField] private GameObject[] objects;


    private void Awake()
    {
        StaticBatchingUtility.Combine(objects,gameObject);
    }

}
