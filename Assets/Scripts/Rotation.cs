using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rotation : MonoBehaviour
{
    [SerializeField] float velocidadRotacion;

    [SerializeField] Axis ejeDeRotacion;
    void Update()
    {
        switch (ejeDeRotacion)
        {
            case Axis.X:
                transform.Rotate(new Vector3(1,0,0) * (velocidadRotacion * Time.deltaTime));
                break;
            case Axis.Y:
                transform.Rotate(new Vector3(0,1,0)*(velocidadRotacion*Time.deltaTime));
                break;
            case Axis.Z:
                transform.Rotate(new Vector3(0,0,1) * (velocidadRotacion * Time.deltaTime));
                break;
        }
    }

    enum Axis
    {
        X,
        Y,
        Z
    }
}
